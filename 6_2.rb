puts "Enter height of the piramid: "
height = gets.chomp
height_result = height.to_i * 2
(1..height_result - 1).each do |n|
  space_count = (height.to_i - n).abs
  asterisk_count = height_result - space_count * 2 - 1
  print " " * space_count + "*" * asterisk_count + "\n"
end
