Занятие 14 - лабораторная работа
Необходимо написать программу-игру "видеопокер", используя как можно больше ООП.
Правила игры:
Игроку раздается 5 карт из колоды 52 карт. Игрок может выбрать любые из этих карт и
заменить их на новые, но только один раз (например, он может заменить только две свои
карты, или все пять, или ни одной). Результирующие карты используются для расчета
выигрыша.

Уровень 1
Необходимо создать класс Card (Карта):
Карта может быть одной из 4 "мастей" и иметь свое "значение" из
RANKS = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K",
"A"]
SUITS = ["\u2660", "\u2665", "\u2666", "\u2663"] # ♠ ♥ ♦ ♣

Символы "\u2660" будут преобразованы в соответствующие при выводе на экран.
Карта должна быть создана с двумя параметрами: масть и значение. Карта должна иметь
метод для вывода себя на экран (например, переопределение to_s). Вывод карты может
выглядеть как-то так:
K♣

Уровень 2
Написать класс Deck, который символизирует колоду карт. При создании колоды,
автоматически создаются все 52 карты (все карты - это комбинации "мастей" и
"значений"). Колода будет иметь такие методы:
● Взять карту из колоды (при этом нужно обеспечить, что из колоды нельзя
вытащить две одинаковые карты)
● Перемешать колоду

Уровень 3
Написать класс Hand, который символизирует набранные карты игрока. Класс Hand имеет
следующие методы:
● Добавить карту в руку
● Отобразить всю руку на экране. Отображать нужно номер карты в руке, и саму
карту. Вывод карт можно сделать например как-то так:
0 | 1 | 2 | 3 | 4 |
3♦ | 10♣ | A♥ | 6♥ | J♦ |
● Можно сделать вывод номеров карт с 1 а не с 0, для удобства
● Изменить карту в руке по номеру позиции (передается номер позиции и карта). При
этом эта карта автоматически заменяет существующую.

Уровень 4
Написать пользовательское взаимодействие для игры, которое должен находиться в
классе Application:
● Раздать 5 карт игроку
● Вывести розданные карты на экран
● Предложить игроку заменить какие-то из них
Введите номера карт, которые вы хотите заменить через пробел
Например, "0 3 4" заменят 0, 3 и 4 карты соответственно
● Обработать ввод пользователя
● Для каждой позиции карты поменять эти карты в руке
● Вывести результат на экран.
