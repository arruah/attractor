filename = "contacts.txt"
contacts_file = File.read(filename)
contacts_count = File.foreach(filename).count
contacts_data = Array.new(contacts_count, {name: "", phone: ""})
#puts contacts_file
count = -1
contacts_file.each_line do |line|
  name, phone = /^([^\d]+)\s+(.+)$/.match(line).captures
#  contacts_data[count][:name] = name # записываем данные в хэш
#  contacts_data[count][:phone] = phone # записываем данные в хэш
  contacts_data.push(name: name, phone: phone)
  count = count + 1
  puts "#{name} #{phone} #{count.to_s}"
end
puts contacts_data
