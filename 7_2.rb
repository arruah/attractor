movies = {
"Interstellar" => {
"John" => 10,
"Jack" => 3
},
"Psyho" => {
"Jack" => 9,
"Jane" => 10
},
"Seven" => {}
}

def greet (string)
  puts string
  print ">"
  @answer = gets.chomp
end

loop do
greet "What do you want to do"
case @answer

when "add movie"
    greet "Please add name of new movie"
    movies[@answer] = {}
    puts "Movie #{@answer} added successfully"
    puts "-------------------"
when "delete movie"
  greet "Please enter the name of the movie you want to delete:" 
  if !movies.has_key? @answer
    puts "Movie #{@answer} not found"
  else
    movies.delete @answer
    puts "movie #{@answer} has been deleted"
  end
when "add rating"
  greet "Please select the movie"
  if !movies.has_key? @answer
    puts "Movie #{@answer} not found"
  else
    #puts "Please enter the name: "
    #name = gets.chomp
    #puts "Please enter your rating for #{@answer}: "
    #rating = gets.chomp
    movie = @answer
    name = greet "Please enter the name: "
    rating = greet "Please enter your rating for user #{name} and movie #{movie}"
    if rating.to_i < 1 or rating.to_i > 10
      puts "Please enter your rating more than 1 and less than 10"
    else
    movies[movie][name]=rating.to_i
    puts "A rating has been added for #{movie}: #{name} rated it #{rating}"
    end
    end
when "list movie rating"
  greet "Please select the movie: "
  if !movies.has_key? @answer
    puts "Movie #{@answer} not found"
  else
    puts "#{@answer} ratings: "
    movies[@answer].each { |key, value| puts "#{key} rates it #{value}"}
    average_rating = movies[@answer].values.map.sum.fdiv( movies[@answer].size)
    puts "Average rating for the movie: " + sprintf("%.2f", average_rating.to_s)
  end
when "list"
  movies.each do |x|
    current_movie = x[0]
    avg_rate = movies[current_movie].values.map.sum.fdiv(movies[current_movie].size)
    if movies[current_movie].empty? 
      puts "Rating is not available for #{x[0]}"
    else
    puts "#{x[0]} is rated #{avg_rate.to_s}" 
    end
  end
when "list sorted"
  sorted_by_ratings = movies.each_with_object({}) do |(name, votes), hash|
    sum = votes.values.sum
    hash[name] = sum.nonzero? ? sum.to_f / votes.count : 0
  end.sort_by(&:last).reverse

  sorted_by_ratings.each do |name, rate|
    if rate.nonzero?
     puts "#{name} is rated #{rate}"
   else
     puts "Rating #{rate} is not available for #{name}"
   end
 end
  puts "-------------------"
when "exit"
  puts "Exiting ..."
  break
else 
  puts "There is no such command"
  puts "~~~~~~~~~~~~~~~~~~~~~~~~"
end
end
