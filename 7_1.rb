contacts = [
  {
    name: "John Doe",
    phone: "0555 555555"
},
{
  name: "Jack Daniels",
  phone: "0543 987978"
}
]

def find_by_name(contacts, search_name)
  contacts.each do |contact|
    if contact[:name] == search_name
      return contact
    end
  end
return nil
end

loop do
  puts "What do you want to do?"
  command = gets.chomp

  case command
  when "list"
    contacts.each do |contact|
      puts "Name: #{contact[:name]}"
      puts "Phone: #{contact[:phone]}"
      puts "-------------------------"
    end
  when "find"
    puts "Who do you want to find?"
    search_name = gets.chomp

    contact = find_by_name(contacts, search_name)

    if contact.nil?
      puts "Contact not found"
    else 
      puts "Name: #{contact[:name]}"
      puts "Phone: #{contact[:phone]}"
    end

  when "edit"
    puts "Please enter the name: "
    edit_name = gets.chomp
    contact = find_by_name(contacts, edit_name)
    
    if contact.nil?
      puts "Contact not found"
      puts "Contact phone has not been updated"
    else
    puts "Please enter the new phone number: "
    new_phone = gets.chomp
    contact[:phone] = new_phone
    puts "Contact phone has been updated"
    end
    puts "------------------------------"
  when "add"
  when "exit"
    puts "Exiting..."
    break
  else
    puts "There is no such command"
  end
      puts "~~~~~~~~~~~~~~~~~~~~~~~~~"
end
