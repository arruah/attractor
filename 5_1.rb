def random
  rand(1 .. 9)
end

def res(a, b)
  a * b
end

a = random
b = random

puts "How many is #{a} * #{b} ?"
result = gets.chomp.to_i
if result == res(a, b) 
  puts "You are correct"       
else 
  puts "You are wrong. Correct answer is #{a} * #{b} = #{res(a, b)}"
end
