def fill_the_array array_size
  values_east = Array.new(array_size.to_i) {|e| e = rand (15..35)}
  values_west = Array.new(array_size.to_i) {|e| e = rand (15..35)}
  values_north = Array.new(array_size.to_i) {|e| e = rand (15..35)}
  values_south = Array.new(array_size.to_i) {|e| e = rand (15..35)}
  puts "meteo station east = " + values_east.to_s
  puts "meteo station west = " + values_west.to_s
  puts "meteo station north = " + values_north.to_s
  puts "meteo station south = " + values_south.to_s
  sum_all = values_east + values_west + values_north + values_south
  average = sum_all.inject {|sum, el| sum + el} / sum_all.size
  puts "Average temperature is: " + average.to_s
end
puts "Enter size of arrays: "
array_size = gets.chomp
puts "Generated arrays of temperatures: "
fill_the_array array_size
