def comparison (first, second)
  if first.length > second.length
  result = first.length - second.length
  elsif first.length < second.length
    result = second.length - first.length
  else result = 0
  end

  return result
end

puts "Enter first string"
first = gets.chomp
puts "Enter second string"
second = gets.chomp

final = comparison first, second

if  final > 0
puts "First string is longer than second by #{final} characters."
elsif final == 0
  puts "Your strings are equal in length!"
else
  puts "Second string is longer than first by #{final} characters."
end
