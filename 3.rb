def age( year_of_birth )
  current_year = Time.now.year
  current_year - year_of_birth.to_f
end

puts "Enter your name: "
name = gets.chomp
puts "Enter your last name: "
lastname = gets.chomp
puts "When were you born? "
year_of_birth = gets.chomp
puts "Where are you from"
place = gets.chomp

client = {:name => name, :lastname => lastname, :year => age(year_of_birth), :place => place }
puts "Hello, #{name.capitalize}. You are #{age(year_of_birth)} years old. You are living in #{place.capitalize}"
