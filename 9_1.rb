def open_file
  filename = "contacts.txt"
  contacts_file = File.read(filename)
  return contacts_file
end

def read_contacts 
  contacts_data = []
  regex = /^([^\d]+)\s+(.+)$/
  contacts_file = open_file
    contacts_file.each_line do |line|
    name, phone = regex.match(line).captures
    contacts_data << ({name: name, phone: phone})
    puts "#{name} #{phone}"
    puts "--------------"
  end
  return contacts_data
end

def add_new(contacts, new_name, new_phone)
  contacts << ({name: new_name, phone: new_phone})
end

def delete_exist(contacts, delete_name)
  contacts.delete_if{|x| x[:name] == delete_name}
end

def find_by_name(contacts, search_name)
  contacts.each do |contact|
    if contact[:name] == search_name
      return contact
    end
  end
return nil
end

contacts = read_contacts

def write_contacts(contacts)
  filename="contacts.txt"
  contacts_file = File.open(filename, "w+")
  contacts.each do |contact|
  contacts_file.write "#{contact[:name]}    #{contact[:phone]}"
  contacts_file.write "\n"
  end
  puts "File was updated"
  contacts_file.close
end

loop do
  puts "What do you want to do?"
  command = gets.chomp

  case command
  when "list"
  read_contacts
  when "write"
    write_contacts(contacts)
  when "show"
    puts contacts
  when "find"
    puts "Who do you want to find?"
    search_name = gets.chomp
    if search_name.include? "," 
      puts "String contains , symbol please enter the name without  , symbol "
    else
    contact = find_by_name(contacts, search_name)
    end

    if contact.nil?
      puts "Contact not found"
    else 
      puts "Name: #{contact[:name]}"
      puts "Phone: #{contact[:phone]}"
    end

  when "edit"
    read_contacts
    puts "Please enter the name: "
    edit_name = gets.chomp
    contact = find_by_name(contacts, edit_name)
    
    if contact.nil?
      puts "Contact not found"
      puts "Contact phone has not been updated"
    else
    puts "Please enter the new phone number: "
    new_phone = gets.chomp
    contact[:phone] = new_phone
    puts "Contact phone has been updated"
    end
    puts "------------------------------"
  when "add"
    puts "Please enter the name: "
    new_name = gets.chomp
    puts "Please enter the phone:"
    new_phone = gets.chomp
    add_new(contacts, new_name, new_phone)
    puts "Name #{new_name} with #{new_phone} added"
  when "delete"
    puts "Please enter the name: "
    delete_name = gets.chomp
    delete_exist(contacts, delete_name)
  when "exit"
    puts "Exiting..."
    break
  else
    puts "There is no such command"
  end
      puts "~~~~~~~~~~~~~~~~~~~~~~~~~"
end
